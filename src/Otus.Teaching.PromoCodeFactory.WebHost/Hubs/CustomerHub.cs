
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Microsoft.Extensions.Logging;
using GrpcServer.Services;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace SignalR.Server
{
    public class CustomerHub : Hub<ICustomerHubClient>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly ILogger<CustomerHub> _logger;

        public CustomerHub(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository, ILogger<CustomerHub> logger)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _logger = logger;
        }

        public async Task<List<CustomerShortResponse>> GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            return response;
        }

        public async Task<CustomerResponse> GetCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var response = new CustomerResponse(customer);
            return response;
        }

        public async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            //�������� ������������ �� �� � ��������� ������� ������
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);
            var response = new CustomerResponse(customer);
            return response;
        }

        public async Task EditCustomers(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) throw new Exception("NotFound");
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            CustomerMapper.MapFromModel(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);
        }

        public async Task DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) throw new Exception("NotFound");

            await _customerRepository.DeleteAsync(customer);
        }
    }

    public interface ICustomerHubClient
    {
        Task<List<CustomerShortResponse>> GetCustomers();
        Task<CustomerResponse> GetCustomer(Guid id);
        Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request);
        Task EditCustomers(Guid id, CreateOrEditCustomerRequest request);
        Task DeleteCustomer(Guid id);
    }
}