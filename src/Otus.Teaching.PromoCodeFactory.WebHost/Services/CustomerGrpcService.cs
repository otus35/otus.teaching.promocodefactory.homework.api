using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace GrpcServer.Services
{
    public class CustomerGrpcService : CustomerGrpc.CustomerGrpcBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly ILogger<CustomerGrpcService> _logger;

        public CustomerGrpcService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository, ILogger<CustomerGrpcService> logger)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _logger = logger;
        }

        public override async Task<CustomerShortResponseGrpc> GetCustomersGrpc(GetCustomersRequestGrpc request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var items = customers.Select(x => new CustomerShortResponseGrpcItem()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var response = new CustomerShortResponseGrpc();
            response.Items.AddRange(items);
            return response;
        }

        public override async Task<GetCustomerResponseGrpc> GetCustomerGrpc(GetCustomerRequestGrpc request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out Guid id))
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                if (customer == null) throw new Exception("NotFound");
                return CustomerMapper.MapToGrpsModel(customer);
            }
            throw new Exception("TryParse ID error");
        }
        public override async Task<GetCustomerResponseGrpc> CreateCustomerGrpc(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            try
            {
                var PreferenceIds = request.PreferenceIds.ToList().Select(Guid.Parse).ToList();
                //�������� ������������ �� �� � ��������� ������� ������
                var preferences = await _preferenceRepository
                    .GetRangeByIdsAsync(PreferenceIds);

                Customer customer = CustomerMapper.MapFromGrpsModel(request, preferences);
                await _customerRepository.AddAsync(customer);
                return CustomerMapper.MapToGrpsModel(customer);
            }
            catch (Exception ee)
            {
                throw new Exception("error", ee);
            }
        }
        public override async Task<EmptyResponse> EditCustomerGrpc(EditCustomerRequestGrpc request, ServerCallContext context)
        {
            try
            {
                Guid id = Guid.Parse(request.Id);

                var customer = await _customerRepository.GetByIdAsync(id);
                if (customer == null) throw new Exception("NotFound");

                var PreferenceIds = request.Customer.PreferenceIds.ToList().Select(Guid.Parse).ToList();
                var preferences = await _preferenceRepository.GetRangeByIdsAsync(PreferenceIds);

                CustomerMapper.MapFromGrpsModel(request.Customer, preferences, customer);
                await _customerRepository.UpdateAsync(customer);
                return new EmptyResponse();
            }
            catch (Exception ee)
            {
                throw new Exception("error", ee);
            }
        }

        public override async Task<EmptyResponse> DeleteCustomerGrpc(DeleteCustomerRequestGrpc request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out Guid id))
            {
                var customer = await _customerRepository.GetByIdAsync(id);

                if (customer == null) throw new Exception("NotFound");

                await _customerRepository.DeleteAsync(customer);

                return new EmptyResponse();
            }
            throw new Exception("TryParse ID error");
        }
    }
}